##3                      === <<TRANSLATIONS>> ===


##3 <<##BBecoming a Translator##b>>
 <<##9To become a translator, you need to access
 [@@https://www.transifex.com/arctic-games/moubootaur-legends|Transifex Website@@], create an account,
 and request to join Arctic Games organization.>>
 <<##9You can then request ##Bjesusalva##b to approve you. If you translate well,
 you will be granted a skin to look like Tux! (Available in two models).>>


##3 <<##BTranslated versus Reviewed##b>>
 <<##9To provide the game in multiple languages, we make use of Machine
 Translation. A Reviewed string means it was manually translated.>>


##3 <<##BQuickly fixing typos##b>>
 <<##9Machine Translation is great at making mistakes. If you are logged on
 Transifex and know the file name (or part of the file name, usually the NPC
 name is enough), you can use ##B@translate <npc name>##b to get a link which
 will allow you to quickly translate it.>>
 <<##9Be warned we take a while to update Translations. Always bug Jesusalva in
 order to get your reviewed translations added faster.>>


##3 <<##BGetting Manual Support##b>>
 <<##9You can ask for help on ##B#world##b chat tab. Be warned: we do not accept
 translation reviews outside transifex. Mostly because we never find time to do
 so, and they end up lost under piles of chat and other priorities.>>

 <<##9You can also highlight @Saulc and @jesusalva for walkthroughs and getting
 added to translation teams in an easier way.>>

 <<##1Important: Prefix every message sent to #world with ##B!translate##b, so
 our #world channel bot is able to translate them to english. This way, others
 will be able to help you!>>


##3 <<##BAre you lost?##b>>
 <<##9If you're lost, you might want to read our [@@help://faq|FAQ@@].>>
 <<##9Otherwise, asking on ##B#world##b has proven highly effective if you can
 wait for a reply.>>

