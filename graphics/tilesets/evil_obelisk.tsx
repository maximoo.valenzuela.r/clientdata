<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="evil_obelisk" tilewidth="64" tileheight="128" tilecount="7" columns="7">
 <image source="evil_obelisk.png" width="448" height="128"/>
 <tile id="0">
  <animation>
   <frame tileid="1" duration="800"/>
   <frame tileid="2" duration="75"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="4" duration="150"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="6" duration="2500"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="4" duration="150"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="2" duration="75"/>
  </animation>
 </tile>
</tileset>
