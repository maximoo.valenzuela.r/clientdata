<?xml version="1.0" encoding="UTF-8"?>
<tileset name="water2" tilewidth="32" tileheight="32" tilecount="165" columns="15">
 <image source="water2.png" width="480" height="352"/>
 <tile id="120">
  <animation>
   <frame tileid="0" duration="120"/>
   <frame tileid="120" duration="120"/>
   <frame tileid="1" duration="1"/>
   <frame tileid="2" duration="120"/>
   <frame tileid="3" duration="120"/>
   <frame tileid="4" duration="120"/>
   <frame tileid="75" duration="1"/>
  </animation>
 </tile>
 <tile id="122">
  <properties>
   <property name="animation-delay0" value="35"/>
   <property name="animation-delay1" value="35"/>
   <property name="animation-delay2" value="35"/>
   <property name="animation-delay3" value="35"/>
   <property name="animation-delay4" value="35"/>
   <property name="animation-frame0" value="5"/>
   <property name="animation-frame1" value="6"/>
   <property name="animation-frame2" value="7"/>
   <property name="animation-frame3" value="8"/>
   <property name="animation-frame4" value="9"/>
  </properties>
 </tile>
 <tile id="135">
  <animation>
   <frame tileid="135" duration="200"/>
   <frame tileid="15" duration="200"/>
   <frame tileid="16" duration="220"/>
   <frame tileid="17" duration="200"/>
   <frame tileid="18" duration="220"/>
   <frame tileid="19" duration="222"/>
  </animation>
 </tile>
 <tile id="150">
  <animation>
   <frame tileid="150" duration="100"/>
   <frame tileid="30" duration="100"/>
   <frame tileid="31" duration="100"/>
   <frame tileid="32" duration="100"/>
   <frame tileid="33" duration="100"/>
   <frame tileid="34" duration="100"/>
  </animation>
 </tile>
</tileset>
